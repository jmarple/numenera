import yaml
import pprint
import itertools
import random

pp = pprint.PrettyPrinter(indent=4)
f = open('objects.yaml')
objects = yaml.safe_load(f)
f = open('abilities.yaml')
abilities = yaml.safe_load(f)
f = open('better_adjectives.yaml')
adjectives = yaml.safe_load(f)
# for i in itertools.product(objects.values(), abilities.values()):
#    print(i)
spells = []
for obj in objects:
    for abl in abilities:
        spell = ""
        if (random.randint(0,1) == 1):
            adj = random.choice(adjectives)
            spell += "\\begin{spell}{" +  adj['name'] + " " + obj['name'] + ' of ' + abl['name'] + " (" + str(random.randint(1,6)) + ')}{}{}{}{}{}{}\n' + obj['description'] + " " + abl['description'] + " " + adj['description']
        else:
            spell += "\\begin{spell}{" +  obj['name'] + ' of ' + abl['name'] + " (" + str(random.randint(1,6)) + ')}{}{}{}{}{}{}\n' + obj['description'] + ' ' + abl['description']

        spell += '\n\end{spell}\n'
        spells.append(spell)

random.shuffle(spells)
for i in spells:
    print(i)
